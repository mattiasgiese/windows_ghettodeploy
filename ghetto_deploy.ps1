﻿# Needed in order to extract archives
Import-Module Microsoft.Powershell.Archive

$bin = '~/bin'
md -Force $bin | Out-Null

# some constants
$autostart_dir = 'C:\Users\Mattias\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup'

if (-Not (Test-Path -Path c:\ProgramData\chocolatey\bin)) {
# first, bootstrap chocolatey
  Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
  Write-Host 'Rerun this script in a fresh shell'
  Exit 0
}
$package_list = './choco_packages.txt'

# install orgy
Write-Output "Trying to install all packages from $package_list"
Get-Content $package_list | foreach {
  # choco list is fucking awful and returns 0 if the package is not installed
  choco list --local-only --exact foo | findstr '0 packages installed' | Out-Null
  if (-Not $?) {
    C:\ProgramData\chocolatey\bin\choco.exe install -y $_
  }
  else {
    Write-Host "$_ already installed"
  }
}

# Try to get w10privacy
$wp_zip = '~\winprivacy.zip'
$wp_config = "$PSScriptRoot\W10Privacy.ini"
If (-Not (Test-Path -Path $wp_zip)) {
  Invoke-WebRequest -Uri https://www.winprivacy.de/app/download/12302828636/W10Privacy.zip?t=1550811328 -OutFile $wp_zip
  Expand-Archive $wp_zip -DestinationPath $bin
}

Invoke-Expression "$bin\W10Privacy.exe /s 1 /config $wp_config"

$ahk = 'C:\Program Files\AutoHotkey\AutoHotkeyU64.exe'
$capslock_mod = '~/capslock_mod.ahk'

Invoke-WebRequest -Uri https://gist.githubusercontent.com/sedm0784/4443120/raw/736e499ad4556eb9ae7551f111ad4350e3097f6b/CapsLockCtrlEscape.ahk -OutFile $capslock_mod

# https://stackoverflow.com/questions/6338015/how-do-you-execute-an-arbitrary-native-command-from-a-string
# Does not seem to work as intended
Invoke-Expression "'&$ahk $capslock_mod'"
